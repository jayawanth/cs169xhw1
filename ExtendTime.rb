class Fixnum
# returns Fixnum, in seconds
  def seconds;  self;                 end
  def minutes;  self * 60;            end
  def hours;    self * 60 * 60;       end
  def days;     self * 24 * 60 * 60;  end

  alias second  seconds
  alias minute  minutes
  alias hour    hours
  alias day     days

  # returns Time, in seconds, displayed in Time format
  def ago;      Time.now - self;      end
  def from_now; Time.now + self;      end
end

class Time
    def at_start_of_year
      Time.local(self.year, 1, 1) # current year, Jan, 1st
    end
end