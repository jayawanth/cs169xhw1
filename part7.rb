class CartesianProduct
  include Enumerable
  def initialize(a_sequence, b_sequence)
    @a_sequence = a_sequence
    @b_sequence = b_sequence
  end

  def each
    @a_sequence.each do |a|
      @b_sequence.each do |b|
        yield [a, b]
      end
    end
  end
end