#
# RPS game/tournament
#

# run a tournament, return the winner
def rps_tournament_winner(matches)
  tournament(matches)   # call recursive version
end

# recurse through the tree of play-offs
def tournament(matches)
  raise WrongNumberOfPlayersError, "matches: #{matches}" unless rps_well_formed(matches)
  if game?(matches)
    return play_game(matches)
  end

  # more play-offs required
  winner1 = tournament(matches[0])
  winner2 = tournament(matches[1])
  tournament([winner1, winner2])
end

# an actual game? e.g. [ ["Armando", "P"], ["Dave", "S"] ]
def play_game(match)
  player1_data, player2_data = match[0], match[1]
  strategy1, strategy2 = player1_data[1], player2_data[1]

  raise NoSuchStrategyError, "'#{strategy1}' in #{player1_data}" unless strategy_ok(strategy1)
  raise NoSuchStrategyError, "'#{strategy2}' in #{player2_data}" unless strategy_ok(strategy2)

  # R beats S; S beats P; and P beats R
  if strategy1 == 'R'
    result =  (strategy2 == 'S')? player1_data: player2_data
  elsif strategy1 == 'S'
    result = (strategy2 == 'P')? player1_data: player2_data
  else  # strategy1 =='P'
    result = (strategy2 == 'R')? player1_data: player2_data
  end
  result
end

# Validation and Error Reporting
class WrongNumberOfPlayersError <  StandardError ; end
class NoSuchStrategyError <  StandardError ; end

# Note:
#   kind_of? and is_a? are synonymous.
#   instance_of? is different - true if the object is an instance of that exact class, not a subclass
def rps_well_formed(matches)
  matches.kind_of?(Array) && matches.count == 2
end

# down to an actual game? e.g. [ ["Armando", "P"], ["Dave", "S"] ]
def game?(match)
  match[0][0].kind_of?(String) && match[1][0].kind_of?(String)
end

def strategy_ok(strategy)
  strategy =~ /^[RPS]$/i
end
