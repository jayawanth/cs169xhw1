# Given an array of words, group words which are anagrams into their own sets
# Example:
#   input: ['cars', 'for', 'potatoes', 'racs', 'four', 'scar', 'creams', 'scream']
#   output: [ ["cars", "racs", "scar"],
#             ["four"],
#             ["for"],
#             ["potatoes"],
#             ["creams", "scream"] ]

def combine_anagrams(words)
  anagrams = Hash.new([])
  words.each do |w|
    index = w.downcase.chars.sort.join
    anagrams[index] += [w]
  end
  anagrams.values
end