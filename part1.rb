#   - ignore case, punctuation, non-word chars
def palindrome?(string)
  string_chars_only = string.gsub(/\W/i, '')    # zap non-word chars in string
  string_chars_only.downcase!
  string_chars_only == string_chars_only.reverse
end

# Note: Hash allocation and initialization required
# scan for words in the given string, count them
def count_words(string)
  words = string.scan /\w+/
  count = Hash.new(0)   # default value = 0
  words.each do |w|
    count[w.downcase] += 1  # ignore case while counting
  end
  count
end

