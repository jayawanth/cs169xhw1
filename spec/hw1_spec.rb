require_relative 'spec_helper'
describe "HW1-Part1" do
  it "palindrome?" do
    palindrome?("").should be_true
    palindrome?("A man, a plan, a canal -- Panama").should be_true
    palindrome?("Madam, I'm Adam!").should be_true
    palindrome?("Abracadabra").should be_false
  end

  it "count_words" do
    count_words("A man, a plan, a canal -- Panama").should eql ({'a' => 3, 'man' => 1, 'canal' => 1, 'panama' => 1, 'plan' => 1})
    count_words("Doo bee doo bee doo").should eql ({'doo' => 3, 'bee' => 2})
  end
end

describe "HW1-Part2" do
  before :all do
    @tournament_data = [
        [
            [["Armando", "P"], ["Dave", "S"]],
            [["Richard", "R"], ["Michael", "S"]],
        ],
        [
            [["Allen", "S"], ["Omer", "P"]],
            [["David E.", "R"], ["Richard X.", "P"]]
        ]
    ]
  end

  it "RPS Simple game, Tournament" do
    rps_tournament_winner([["Armando", "P"], ["Dave", "S"]]).should eql (["Dave", "S"])
    rps_tournament_winner(@tournament_data).should eql ["Richard", "R"]
  end
end

describe "HW1-Part3" do
  before :all do
    @word_set1 = ["rats", "tars", "star", "dictionary", "indicatory"]
    @result_word_set1 = [
        ["rats", "tars", "star"],
        ["dictionary", "indicatory"]]
    @word_set2 = ['cars', 'for', 'potatoes', 'racs', 'four', 'scar', 'creams', 'scream']
    @result_word_set2 = [
        ["cars", "racs", "scar"],
        ["for"],
        ["potatoes"],
        ["four"],
        ["creams", "scream"]]
  end
  it "combine_anagrams" do
    combine_anagrams(@word_set1).should eql @result_word_set1
    combine_anagrams(@word_set2).should eql @result_word_set2
  end
end

describe "HW1-Part4 Basic Object Oriented Programming" do
  it "Class Dessert, Jellybean" do
    "Too simple, skipping"
  end
end

describe "HW1-Part5 Tests OOP, Metaprogramming, Open Classes, Duck Typing" do
  before :all do
    class Foo
      attr_accessor_with_history :bar
    end
  end
  it "attr_accessor_with_history" do
    f = Foo.new # => #<Foo:0x127e678>
    f.bar = 3 # => 3
    f.bar = :wowzo # => :wowzo
    f.bar = 'boo!' # => 'boo!'
    f.bar_history.should eql [nil, 3, :wowzo, 'boo!']
  end

  it "attr_accessor_with_history maintain history for each object" do
    f = Foo.new
    f.bar = 1
    f.bar = 2
    f = Foo.new
    f.bar = 4
    f.bar_history.should eql [nil, 4]
  end
end

describe "HW1-Part6 Advanced OOP, Metaprogramming, Open Classes and Duck Typing" do
  it "currency conversions" do
    # returns value in cents
    1.dollar.should eql 100
    10.yens.should eql 13.0
    10.euros.should eql 1292.0
    10.rupees.should eql 19.0

    # conversions
    13.dollars.in(:yens).should eql 1000.0
    1292.dollars.in(:euros).should be_within(0.5).of(1000.0)
    19.dollars.in(:rupees).should be_within(0.5).of(1000.0)
  end
  it "palindrome? for all String objects" do
    "".palindrome?.should be_true
    "A man, a plan, a canal -- Panama".palindrome?.should be_true
    "Madam, I'm Adam!".palindrome?.should be_true
    "Abracadabra".palindrome?.should be_false
  end

  it "palindrome? module Enumerable" do
    [1, 2, 3, 2, 1].palindrome?.should be_true
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0].palindrome?.should be_true
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0].palindrome?.should be_true
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 7, 6, 5, 4, 3, 2, 1, 0].palindrome?.should be_false
  end
end

describe "HW1-Part7 Iterators, Blocks, Yield" do
  before :all do
    @a = [:a, :b, :c]
    @b = [4, 5]
    @aXb = CartesianProduct.new(@a, @b)
    @c = []
    @aXc = CartesianProduct.new(@a, @c)
  end
  it "Cartesian Product" do
    cross_product = []
    @aXb.each do |a, b|
      cross_product << [a, b]
    end
    cross_product.should eql [[:a, 4], [:a, 5], [:b, 4], [:b, 5], [:c, 4], [:c, 5]]

    cross_product = []
    @aXc.each do |a, c|
      cross_product << [a, c]
    end
    cross_product.should eql []
  end
end
