class Numeric
  @@currencies = {'yens' => 0.013,  'euros' => 1.292, 'rupees' => 0.019, 'dollars' => 1,
                  'yen' => 0.013,   'euro' => 1.292,  'rupee' => 0.019, 'dollar' => 1}
  # returns Fixnum, in cents
  def dollars;  self*100;                         end
  def euros;    self*100*@@currencies['euros'];   end
  def yens;     self*100*@@currencies['yens'];    end
  def rupees;   self*100*@@currencies['rupees'];  end

  # handle singular version too e.g. 1.euro or 1.rupee
  alias dollar  dollars
  alias euro   euros
  alias yen     yens
  alias rupee   rupees

  # conversions e.g. 5.dollars.in(:euros)
  def in(to_currency)
    if @@currencies.has_key? to_currency.to_s
      self/(@@currencies[to_currency.to_s]*100)
    else
      self
    end
  end
end

class String
  def palindrome?
    string_chars_only = self.gsub(/\W/i, '')  # zap non-word chars in string
    string_chars_only.downcase!
    string_chars_only == string_chars_only.reverse
  end
end

module Enumerable
  def palindrome?
    a = self.to_a
    a == a.reverse
  end
end