module CheckedAttributes
  def attr_checked(attr_symbol, &block)
    attr_name = attr_symbol.to_s # make sure it's a string
    attr_reader attr_name # reader, same as before

    # attr_writer with validation using given block
    define_method "#{attr_name}=" do |value|
      result = block.call(value)
      if result
        instance_variable_set "@#{attr_name}", value
      else
        puts "validation failed:#{value}"
      end
    end
  end
end

class C
  include CheckedAttributes
  attr_checked :age do |value|
    value >= 18
  end
end
