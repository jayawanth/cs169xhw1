class Class
  def attr_accessor_with_history(attr_symbol)
    attr_name = attr_symbol.to_s # make sure it's a string
    attr_reader attr_name # reader, same as before

    # redefine attr_writer, adds value to history
    class_eval %Q{
                  def #{attr_name}=(value)
                    @#{attr_name} = value           # same as attr_writer

                    # manage history
                    if @#{attr_name}_history == nil
                      @#{attr_name}_history = [nil] # as per spec - nil is part of history
                    end
                    @#{attr_name}_history += [value]
                  end
                }
    # attr_reader attr_name+"_history" # create bar_history getter for :bar
    class_eval %Q{
                  def #{attr_name}_history    # similar to reader
                    return [nil] if @#{attr_name}_history == nil  # as per spec - return array always
                    @#{attr_name}_history
                  end
                }
  end
end



